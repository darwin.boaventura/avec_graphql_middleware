const controllers = {
	user: require('../controllers/User.controller')
};

const UserResolver = {
	createUser: async (_, { name, gender, cpf, phone, email, password }, { dataSources }) => {
		const SVIPREquest = await dataSources.svip.createUser({ name, gender, cpf, phone, email, password });
		await dataSources.vtex.createUser({name, gender, cpf, phone, email});

		if (SVIPREquest.code === 200) {
			const userSVIP = SVIPREquest.data.client;

			return {
				success: true,
				user: {
					id: userSVIP.id,
					name: userSVIP.nome,
					gender: userSVIP.sexo,
					cpf: userSVIP.cpf,
					phone: userSVIP.telefone,
					email: userSVIP.email
				}
			};
		} else {
			return {
				success: false,
				message: SVIPREquest.data.validation[0]
			};
		}
	},

	updateUser: async (_, { name, gender, cpf, phone, photo, password }, { dataSources, user, token }) => {
		await controllers.user.verifyIfHasTokenAndUser({user, token});
		await controllers.user.verifyIfTokenHasExpired({user, token});

		const SVIPREQUEST = await dataSources.svip.updateUser({ userId: user.id, tokenSVIP: user.token,name, gender, cpf, phone, password });
		const { code } = SVIPREQUEST;

		const getUserREQUEST = await dataSources.svip.getUserByEmail(user.email);
		const userSVIP = getUserREQUEST.data.client;

		dataSources.vtex.updateUser({ user, name, gender, phone, cpf })
		.then(() => {})
		.catch((e) => { console.log('Error VTEX:', e); });

		if (code === 200) {
			if (photo) {
				await UserResolver.updateUserPicture(null, { photo }, { dataSources, user, token });
			}

			return {
				success: true,
				message: 'Os dados foram atualizados com sucesso!',
				user: {
					name: userSVIP.nome,
					gender: userSVIP.sexo,
					cpf: userSVIP.cpf,
					phone: userSVIP.telefone,
					email: userSVIP.email
				}
			}
		} else {
			return {
				success: false,
				message: SVIPREQUEST.data.validation[0]
			}
		}
	},

	validateUser: async (_, { email }, { dataSources }) => {
		const SVIPRequest = await dataSources.svip.getUserByEmail(email);
		const { code } = SVIPRequest;

		if (code === 200) {
			const userSVIP = SVIPRequest.data.client;

			if (userSVIP.password) {
				if (userSVIP.email_verificado === 0) {
					return {
						success: false,
						message: 'cliente_nao_verificou_email',
						user: {
							id: userSVIP.id,
							name: userSVIP.nome
						}
					};
				}

				return {
					success: true,
					message: 'cliente_encontrado',
					user: {
						id: userSVIP.id,
						name: userSVIP.nome
					}
				}
			} else {
				return {
					success: false,
					message: 'cliente_sem_senha',
					user: {
						id: userSVIP.id,
						name: userSVIP.nome
					}
				}
			}
		} else if (code === 404) {
			return {
				success: true,
				message: 'cliente_nao_encontrado'
			}
		} else {
			const { message } = SVIPRequest;

			return {
				success: false,
				message: message
			};
		}
	},

	dispatchEmailVerification: async (_, { userId, email }, { dataSources }) => {
		const { code, message } = await dataSources.svip.dispatchEmailVerification({ userId, email });

		return {
			success: Boolean(code === 200),
			message: message
		};
	},

	validateEmailVerificationCode: async (_, { code, email }, { dataSources }) => {
		const RequestSVIP = await dataSources.svip.validateEmailVerificationCode({ code, email });

		return {
			success: Boolean(RequestSVIP.code === 200),
			message: RequestSVIP.message || 'A confirmação foi realizada com sucesso!'
		}
	},

	dispatchMessageVerification: async (_, { userId, phone }, { dataSources }) => {
		const { code, message } = await dataSources.svip.dispatchEmailVerification({ userId, phone });

		return {
			success: Boolean(code === 200),
			message: message
		};
	},

	validateMessageVerificationCode: async (_, { code, phone }, { dataSources }) => {
		const RequestSVIP = await dataSources.svip.validateMessageVerificationCode({ code, phone });

		return {
			success: Boolean(RequestSVIP.code === 200),
			message: RequestSVIP.message
		};
	},

	login: async (_, { email, password }, { dataSources }) => {
		const loginSVIP = await dataSources.svip.login({ email, password });

		if (loginSVIP && loginSVIP.code === 200) {
			const userSVIP = loginSVIP.data.client;

			if (userSVIP.email_verificado === 0) {
				return {
					success: false,
					message: loginSVIP.message
				};
			}

			return await controllers.user.handleSuccessfulLogin(loginSVIP, dataSources);
		}

		return {
			success: false,
			message: loginSVIP.message || loginSVIP.data.validation[0]
		};
	},

	loginSocialNetwork: async (_, { token, provider, email, name, gender }, { dataSources }) => {
		const loginSVIP = await dataSources.svip.loginSocialNetwork({ token, provider, email, name, gender });
		const { code, statusCode } = loginSVIP;

		if (code === 404 || statusCode === 404 || code === 401 || statusCode === 401 || code === 403 || statusCode === 403) {
			return {
				success: false,
				message: 'Falha ao tentar autenticar usuário'
			}
		} else if (code === 200 || statusCode === 200) {
			return await controllers.user.handleSuccessfulLogin(loginSVIP, dataSources);
		} else {
			return {
				success: false,
				message: loginSVIP.message
			}
		}
	},

	updateUserPicture: async (_, { photo }, { dataSources, user, token }) => {
		await controllers.user.verifyIfHasTokenAndUser({user, token});
		await controllers.user.verifyIfTokenHasExpired({user, token});

		const RequestSVIP = await dataSources.svip.updateUserPicture({ photo, token: user.token });

		if (RequestSVIP.code !== 200) {
			return {
				success: false,
				message: 'Ocorreu um erro!'
			}
		} else {
			return {
				success: true,
				message: RequestSVIP.data.filePath
			}
		}
	}
};

module.exports = UserResolver;
