const controllers = {
	user: require('../controllers/User.controller')
};

const ListResolver = {
	getListById: async (_, { listId }, { dataSources }) => {
		return await dataSources.list.getListById({ listId });
	},

	createList: async (_, { title, description, private, background }, { dataSources, token, user }) => {
		await controllers.user.verifyIfHasTokenAndUser({ user, token });
		await controllers.user.verifyIfTokenHasExpired({ user, token });

		const checksIfExistsListWithUserIdAndTitle = await dataSources.list.checksIfExistsListWithUserIdAndTitle({ userId: user.id, title });

		if (checksIfExistsListWithUserIdAndTitle) {
			return {
				success: false,
				message: `O usuário já possuí uma lista com o nome ${title}`
			};
		} else {
			const createList = await dataSources.list.createList({ userId: user.id, title, description, private, background });

			if (!createList) {
				console.log('Error:', createList);

				return {
					success: false,
					message: 'Ocorreu um erro, por favor, tente novamente'
				};
			}

			return {
				success: true,
				message: 'A lista foi criada com sucesso!',
				list: createList
			}
		}
	},

	deleteList: async (_, { listId }, { dataSources, token, user }) => {
		await controllers.user.verifyIfHasTokenAndUser({ user, token });
		await controllers.user.verifyIfTokenHasExpired({ user, token });

		await dataSources.list.deleteList({ userId: user.id, listId });

		return {
			success: true,
			message: `A lista ${listId} foi removida com sucesso!`
		}
	},

	updateList: async (_, { listId, title, description, private, background }, { dataSources, token, user }) => {
		await controllers.user.verifyIfHasTokenAndUser({ user, token });
		await controllers.user.verifyIfTokenHasExpired({ user, token });

		await dataSources.list.updateList({ userId: user.id, listId, title, description, private, background });

		return {
			success: true,
			message: 'A lista foi atualizada com sucesso!'
		};
	},

	addProductsToList: async (_, { listId, products }, { dataSources, token, user }) => {
		await controllers.user.verifyIfHasTokenAndUser({ user, token });
		await controllers.user.verifyIfTokenHasExpired({ user, token });

		await dataSources.list.addProductsToList({ userId: user.id, listId, products });

		return {
			success: true,
			message: 'Os produtos foram adicionados com sucesso!'
		}
	},

	deleteProductsOfAList: async (_, { listId, products }, { dataSources, token, user }) => {
		await controllers.user.verifyIfHasTokenAndUser({ user, token });
		await controllers.user.verifyIfTokenHasExpired({ user, token });

		await dataSources.list.deleteProductsOfAList({ userId: user.id, listId, products });

		return {
			success: true,
			message: 'Os produtos foram removidos com sucesso!'
		}
	}
};

module.exports = ListResolver;
