const lodash = require('lodash');
const helpers = {
	array: require('../helpers/Array.helper')
};

module.exports = {
	getProductById: async (_, { id }, { dataSources }) => {
		return dataSources.vtex.getProductById({ id });
	},

	getProducts: async (_, { filters, orderBy, pagination }, { dataSources }) => {
		return dataSources.vtex.getProducts({ filters, orderBy, pagination });
	},

	getProductsByBrandList: async (_, { brandList }, { dataSources }) => {
		const products = [];

		await helpers.array.asyncForEach(brandList, async (brandId) => {
			products.push(await dataSources.vtex.getProductsOfABrand({ brandId }));
		});

		return lodash.flatten(products);
	}
};
