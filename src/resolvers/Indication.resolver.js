const controllers = {
	user: require('../controllers/User.controller'),
	email: require('../controllers/Email.controller')
};

const IndicationResolver = {
	createIndication: async (_, { userEmail, listId, uri }, { dataSources, user, token }) => {
		await controllers.user.verifyIfHasTokenAndUser({ user, token });
		await controllers.user.verifyIfTokenHasExpired({ user, token });

		const getIndication = await dataSources.indication.getIndication({ professionalId: user.id, userEmail, listId });

		if (!getIndication) {
			const indication = await dataSources.indication.createIndication({ professionalId: user.id, userEmail, listId, uri });

			return {
				success: true,
				message: 'A indicação foi realizada com sucesso!',
				indication: {
					_id: indication._id,
					user: indication.userEmail,
					list: indication.listId,
					professional: indication.professionalId,
					uri: indication.uri,
					sentAt: indication.sentAt
				}
			};
		} else {
			return {
				success: false,
				message: 'Você já indicou essa lista para esse cliente!'
			};
		}
	}
};

module.exports = IndicationResolver;
