const mongoose = require('mongoose');

const TokenModel = mongoose.model('Token', mongoose.Schema({
	userId: {
		type: String,
		unique: true
	},
	token: {
		type: String,
		unique: true
	}
}));

module.exports = TokenModel;
