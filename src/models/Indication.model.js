const mongoose = require('mongoose');

const IndicationModel = mongoose.model('Indication', mongoose.Schema({
	listId: {
		type: String,
		required: true
	},
	userEmail: {
		type: String,
		required: true
	},
	professionalId: {
		type: String,
		required: true
	},
	uri: String,
	sentAt: {
		type: Date,
		default: new Date()
	}
}));

module.exports= IndicationModel;
