const mongoose = require('mongoose');

const ListModel = mongoose.model('List', mongoose.Schema({
	userId: String,
	title: String,
	description: String,
	private: {
		type: Boolean,
		default: false
	},
	background: {
		image: String,
		color: String
	},
	products: [String]
}));

module.exports = ListModel;
