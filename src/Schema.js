const {gql} = require('apollo-server');
const schemas = {
	product: require('./schemas/Product.schema'),
	installment: require('./schemas/Installment.schema'),
	address: require('./schemas/Address.schema'),
	user: require('./schemas/User.schema'),
	list: require('./schemas/List.schema'),
	indication: require('./schemas/Indication.schema'),

	response: {
		login: require('./schemas/Responses/LoginResponse.schema'),
		defaultResponse: require('./schemas/Responses/DefaultResponse.schema'),
		createUser: require('./schemas/Responses/CreateUserResponse.schema'),
		createList: require('./schemas/Responses/CreateListResponse.schema'),
		validateUser: require('./schemas/Responses/ValidateUserResponse.schema'),
		createIndication: require('./schemas/Responses/CreateIndicationResponse.schema')
	},
	enums: {
		gender: require('./schemas/Enums/Gender.enum'),
		vtex: require('./schemas/Enums/Vtex.enum')
	},
	inputs: {
		vtex: require('./schemas/Inputs/Vtex.schema'),
		list: require('./schemas/Inputs/List.schema')
	}
};

const typeDefs = gql`
	type Query {
		# Product
		getProductById(id: ID!): Product
		getProducts(filters: VtexProductsFilters, orderBy: OrderByVtex, pagination: VtexPagination): [Product]
		getProductsByBrandList(brandList: [String]): [Product]
	
		# List
		getListById(listId: ID): List
		
		# User
		validateUser(email: String!): ValidateUserResponse
		login(email: String!, password: String): LoginResponse
		loginSocialNetwork(token: String!, provider: String!, email: String!, name: String!, gender: Gender): LoginResponse
	}
	
	type Mutation {
		# User
		createUser(name: String, gender: Gender, cpf: String, phone: String!, email: String!, password: String!): CreateUserResponse
		updateUser(name: String, gender: Gender, cpf: String, phone: String, photo: Upload, password: String): CreateUserResponse
		updateUserPicture(photo: Upload!): DefaultResponse
		
		dispatchEmailVerification(userId: String!, email: String!): DefaultResponse
		validateEmailVerificationCode(code: ID!, email: String!): DefaultResponse
		
		dispatchMessageVerification(userId: ID!, phone: String!): DefaultResponse
		validateMessageVerificationCode(code: String!, phone: String!): DefaultResponse
		
		# List
		createList(title: String!, description: String, private: Boolean, background: ListBackgroundInput): CreateListResponse
		deleteList(listId: ID!): DefaultResponse
		updateList(listId: ID!, title: String, description: String, isPrivate: Boolean, background: ListBackgroundInput): DefaultResponse
		addProductsToList(listId: ID, products: [ID]): DefaultResponse
		deleteProductsOfAList(listId: ID, products: [String]): DefaultResponse
		
		# Indication
		createIndication(userEmail: String!, listId: ID!, uri: String): CreateIndicationResponse
	}
	
	${schemas.user}
	${schemas.list}
	${schemas.product}
	${schemas.address}
	${schemas.installment}
	${schemas.indication}
	
	# Response's
	${schemas.response.defaultResponse}
	${schemas.response.login}
	${schemas.response.createUser}
	${schemas.response.validateUser}
	${schemas.response.createList}
	${schemas.response.createIndication}
	
	# Enum's
	${schemas.enums.gender}
	${schemas.enums.vtex}
	
	# Input's
	${schemas.inputs.vtex}
	${schemas.inputs.list}
`;

module.exports = typeDefs;
