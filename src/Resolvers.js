const resolvers = {
	user: require('./resolvers/User.resolver'),
	product: require('./resolvers/Product.resolver'),
	list: require('./resolvers/List.resolver'),
	indication: require('./resolvers/Indication.resolver'),
};

const helpers = {
	array: require('./helpers/Array.helper')
};

module.exports = {
	Query: {
		// Product
		getProductById: resolvers.product.getProductById,
		getProducts: resolvers.product.getProducts,
		getProductsByBrandList: resolvers.product.getProductsByBrandList,

		// User
		validateUser: resolvers.user.validateUser,
		login: resolvers.user.login,
		loginSocialNetwork: resolvers.user.loginSocialNetwork,

		// List
		getListById: resolvers.list.getListById
	},

	Mutation: {
		// User
		createUser: resolvers.user.createUser,
		updateUser: resolvers.user.updateUser,
		updateUserPicture: resolvers.user.updateUserPicture,
		dispatchEmailVerification: resolvers.user.dispatchEmailVerification,
		validateEmailVerificationCode: resolvers.user.validateEmailVerificationCode,
		dispatchMessageVerification: resolvers.user.dispatchMessageVerification,
		validateMessageVerificationCode: resolvers.user.validateMessageVerificationCode,

		// List
		createList: resolvers.list.createList,
		deleteList: resolvers.list.deleteList,
		updateList: resolvers.list.updateList,
		addProductsToList: resolvers.list.addProductsToList,
		deleteProductsOfAList: resolvers.list.deleteProductsOfAList,

		// Indication
		createIndication: resolvers.indication.createIndication
	},

	Product: {
		images: (parent, { limit, offset }) => {
			return helpers.array.sliceData(parent.images, limit, offset);
		},
		categories: (parent, { limit, offset }) => {
			return helpers.array.sliceData(parent.categories, limit, offset);
		},
		items: (parent, { limit, offset }) => {
			return helpers.array.sliceData(parent.items, limit, offset);
		}
	},

	ProductItem: {
		images: (parent, { limit, offset }) => {
			return helpers.array.sliceData(parent.images, limit, offset);
		},
		sizes: (parent, { limit, offset }) => {
			return helpers.array.sliceData(parent.sizes, limit, offset);
		},
		variations: (parent, { limit, offset }) => {
			return helpers.array.sliceData(parent.variations, limit, offset);
		},
		sellers: (parent, { limit, offset }) => {
			return helpers.array.sliceData(parent.sellers, limit, offset);
		}
	},

	ProductSeller: {
		installments: (parent, { limit, offset }) => {
			return helpers.array.sliceData(parent.installments, limit, offset);
		},
		gifts: (parent, { limit, offset }) => {
			return helpers.array.sliceData(parent.gifts, limit, offset);
		}
	},

	List: {
		products: (parent, args, { dataSources }) => {
			const products = parent.products.map(async (id) => {
				return await resolvers.product.getProductById(null, { id }, { dataSources });
			});

			return products;
		}
	},

	Indication: {
		list: async (parent, args, { dataSources }) => {
			const listId = parent.list;

			return await dataSources.list.getListById({ listId });
		},

		professional: async (parent, args, { user }) => {
			return {
				name: user.nome,
				email: user.email
			};
		},

		user: async (parent, args, { dataSources }) => {
			const REQUESTSVIP = await dataSources.svip.getUserByEmail(parent.user);
			const user = REQUESTSVIP.data.client;

			return {
				name: user.nome,
				email: user.email
			};
		}
	}
};
