const Address = `
	type Address {
		street: String
		number: String
		neighborhood: String
		city: String
		state: String
		cep: String
	}
`;

module.exports = Address;
