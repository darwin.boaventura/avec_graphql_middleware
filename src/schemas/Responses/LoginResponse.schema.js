const LoginResponseSchema = `
	type LoginResponse {
		success: Boolean
		message: String
		token: String
	}
`;

module.exports = LoginResponseSchema;
