const CreateListResponse = `
	type CreateListResponse {
		success: Boolean
		message: String
		list: List
	}
`;

module.exports = CreateListResponse;
