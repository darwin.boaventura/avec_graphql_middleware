const DefaultResponse = `
	type DefaultResponse {
		success: Boolean
		message: String
	}
`;

module.exports = DefaultResponse;
