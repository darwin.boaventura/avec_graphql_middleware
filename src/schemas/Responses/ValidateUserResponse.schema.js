const ValidateUserResponse = `
	type ValidateUserResponse {
		success: Boolean
		message: String
		user: UserValidateUserResponse
	}
	
	type UserValidateUserResponse {
		id: ID
		name: String
	}
`;

module.exports = ValidateUserResponse;
