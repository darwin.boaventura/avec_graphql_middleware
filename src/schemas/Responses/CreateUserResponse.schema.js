const CreateUserResponse = `
	type CreateUserResponse {
		success: Boolean
		message: String
		user: User
	}
`;

module.exports = CreateUserResponse;
