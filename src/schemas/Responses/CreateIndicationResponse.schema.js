const CreateIndicationResponse = `
	type CreateIndicationResponse {
		success: Boolean
		message: String
		indication: Indication
	}
`;

module.exports = CreateIndicationResponse;
