const UserSchema = `
	type User {
		id: ID
		name: String
		nickName: String
		email: String
		status: Boolean
		photo: String
		birthDay: String
		phone: String
		cellphone: String
		rg: String
		cpf: String
		gender: String
		address: Address
		newsletter: UserNewsletter
		verifications: UserVerifications
	}
	
	type UserNewsletter {
		email: UserNewsletterEmail
		sms: UserNewsletterEmail
	}
	
	type UserNewsletterEmail {
		partner: Boolean
		news: Boolean
		scheduled: Boolean
	}
	
	type UserVerifications {
		email: Boolean
		cellphone: Boolean
	}
`;

module.exports = UserSchema;
