module.exports = `
	enum OrderByVtex {
		OrderByPriceDESC
		OrderByPriceASC
		OrderByTopSaleDESC
		OrderByReviewRateDESC
		OrderByNameASC
		OrderByNameDESC
		OrderByReleaseDateDESC
		OrderByBestDiscountDESC
	}
`;
