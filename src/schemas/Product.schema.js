module.exports = `
	type Product {
		id: ID!
		name: String
		fullName: String
		seller: String
		referenceCode: String
		images(limit: Int, offset: Int): [ProductImage]
		description: String
		categories(limit: Int, offset: Int): [String]
		price: ProductPrice
		items(limit: Int, offset: Int): [ProductItem]
		uri: String
	}

	type ProductImage {
		id: String!
		uri: String
		name: String
	}
	
	type ProductItem {
		id: ID!
		name: String
		fullName: String
		measurementUnit: String
		unitMultiplier: Int
		images(limit: Int, offset: Int): [ProductImage]
		sizes(limit: Int, offset: Int): [String]
		variations(limit: Int, offset: Int): [String]
		sellers(limit: Int, offset: Int): [ProductSeller]
	}
	
	type ProductPrice {
		original: Float
		withDiscount: Float
		discounts: [String]
		list: Float
	}
	
	type ProductSeller {
		id: ID!
		name: String
		isDefaultSeller: Boolean
		installments(limit: Int, offset: Int): [Installment]
		price: ProductPrice
		gifts(limit: Int, offset: Int): [String],
		availableQuantity: Int
		tax: Int
	}
`;
