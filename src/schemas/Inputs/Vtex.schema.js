module.exports = `
	input VtexPagination {
		from: Int
		to: Int
	}
	
	input VtexProductsFilters {
		termToSearch: String
		categoryId: ID
		brandId: ID
		specificationId: ID
		collectionId: ID
		priceRange: VtexPriceRangeFilter
		salesChannel: String
		sellerId: ID
	}
	
	input VtexPriceRangeFilter {
		minPrice: Float
		maxPrice: Float
	}
`;
