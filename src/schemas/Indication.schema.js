const IndicationSchema = `
	type Indication {
		_id: ID
		list: List
		user: User
		professional: User
		uri: String
		sentAt: String
	}
`;

module.exports = IndicationSchema;
