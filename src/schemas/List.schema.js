module.exports = `
	type List {
		_id: ID
		userId: String
		title: String
		description: String
		private: Boolean
		products: [Product]
		background: ListBackground
	}
	
	type ListBackground {
		color: String
		image: String
	}
`;
