module.exports = `
	type Installment {
		value: Float
		name: String
		interestRate: Int
		valueWithInterestRate: Float
		numberOfInstallments: Int
		paymentSystemName: String
		paymentSystemGroupName: String
	}
`;
