const models = {
	token: require('../models/Token.model')
};

const TokenService = {
	getToken: ({ userId, token }) => {
		return models.token.findOne({ userId, token });
	},

	saveToken: (userId, token) => {
		return models.token.updateOne({ userId }, { $set: { token } }, { upsert: true });
	}
};

module.exports = TokenService;
