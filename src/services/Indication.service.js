const models = {
	indication: require('../models/Indication.model')
};

const services = {
	list: require('../services/List.service')
};

const IndicationService = {
	getIndication: async ({ professionalId, userId, listId }) => {
		return models.indication.findOne({ professionalId, userId, listId });
	},

	createIndication: async ({ professionalId, userEmail, listId, uri }) => {
		const list = await services.list.getListById({ listId });

		if (list) {
			if (list.userId == professionalId) {
				const indication = new models.indication({
					professionalId,
					userEmail,
					listId,
					uri
				});

				return indication.save();
			} else {
				throw new Error('A lista informada não pertence ao profissional.');
			}
		} else {
			throw new Error('A lista informada não existe.');
		}
	}
};

module.exports = IndicationService;
