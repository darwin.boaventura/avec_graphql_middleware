const _ = require('lodash');

const models = {
	list: require('../models/List.model')
};
const helpers = {
	object: require('../helpers/Object.helper')
};

const ListService = {
	checksIfExistsListWithUserIdAndTitle: ({ userId, title }) => {
		return models.list.findOne({ userId, title });
	},

	getListById: ({ listId }) => {
		return models.list.findOne({ _id: listId });
	},

	createList: ({ userId, title, description, private, background }) => {
		const List = new models.list({ userId, title, description, private, background });

		return List.save();
	},

	deleteList: async ({ userId, listId }) => {
		return models.list.deleteOne({ userId, _id: listId });
	},

	updateList: async ({ userId, listId, title, description, private, background }) => {
		const list = await models.list.findOne({ userId, _id: listId });

		if (!list) {
			throw new Error(`Não existe lista cadastra para esse usuário com ID ${listId}`);
		} else {
			const data = helpers.object.removeEmptyKeys({
				title,
				description,
				private,
				background: helpers.object.removeEmptyKeys(background)
			});

			return models.list.updateOne({ userId, _id: listId }, { $set: data });
		}
	},

	addProductsToList: async ({ userId, listId, products }) => {
		const list = await models.list.findOne({ userId, _id: listId });

		if (!list) {
			throw new Error(`Não existe lista cadastra para esse usuário com ID ${listId}`);
		} else {
			let dataForUpdateList = list;

			if (products) {
				products.forEach((product) => {
					dataForUpdateList.products.push(product);
				});

				dataForUpdateList.products = _.uniq(dataForUpdateList.products);

				return models.list.updateOne({ _id: listId }, dataForUpdateList);
			}
		}
	},

	deleteProductsOfAList: async ({ userId, listId, products }) => {
		const list = await models.list.findOne({ userId, _id: listId });

		if (!list) {
			throw new Error(`Não existe lista cadastra para esse usuário com ID ${listId}`);
		} else {
			return models.list.updateOne({ _id: listId }, { $set: { products: _.difference(list.products, products) } });
		}
	}
};

module.exports = ListService;
