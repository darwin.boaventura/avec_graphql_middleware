const {RESTDataSource} = require('apollo-datasource-rest');

const controllers = {
	product: require('../controllers/Product.controller')
};
const helpers = {
	object: require('../helpers/Object.helper')
};

class VtexService extends RESTDataSource {
	constructor() {
		super();

		this.accountName = process.env.VTEX_ACCOUNT_NAME;
		this.appKey = process.env.VTEX_APP_KEY;
		this.appToken = process.env.VTEX_APP_TOKEN;
		this.environment = process.env.VTEX_ENVIRONMENT;
		this.contentType = process.env.VTEX_CONTENT_TYPE;
		this.defaultOrderBy = process.env.VTEX_DEFAULT_ORDER_BY;
		this.baseURL = `https://${this.accountName}.${this.environment}.com.br/api/`;
	}

	willSendRequest(request) {
		request.headers.set('X-VTEX-API-AppKey', this.appKey);
		request.headers.set('X-VTEX-API-AppToken', this.appToken);
		request.headers.set('Content-Type', this.contentType);
	}

	// Products

	async getProductById({ id }) {
		const response = await this.get(`catalog_system/pub/products/search?fq=productId:${id}`);

		return response && response.length ? controllers.product.convertVtexPayloadToSchema(response[0]) : [];
	}

	async getProducts({ filters, orderBy = this.defaultOrderBy, pagination }) {
		const products = await this.get('catalog_system/pub/products/search', {
			'ft': filters.termToSearch ? filters.termToSearch : '',
			'fq': controllers.product.generateQueryStringsFilters(filters),
			'O': orderBy,
			...controllers.product.gerenateQueryStringsPagination(pagination)
		});

		if (products && products.length) {
			return products.map((product) => {
				return controllers.product.convertVtexPayloadToSchema(product);
			});
		}
	}

	async getProductsOfABrand({ brandId }) {
		const products = await this.get(`catalog_system/pub/products/search?fq=brandId:${brandId}`);

		if (products && products.length) {
			return products.map((product) => {
				return controllers.product.convertVtexPayloadToSchema(product);
			});
		}
	}

	// Users

	async createUser({name, email, phone, gender, cpf}) {
		const names = name.split(' ');
		const firstName = names[0];
		const lastName = names && names[1] ? names[1] : '';

		await this.post('license-manager/users/', { name, email });

		return this.post('dataentities/CL/documents', { firstName, lastName, email, phone, gender, document: cpf });
	}

	updateUser({ user, name, gender, phone, cpf }) {
		const firstName = name.split(" ")[0];
		const lastName = name.split(" ")[1];

		const data = helpers.object.removeEmptyKeys({
			email: user.email,
			firstName: firstName,
			lastName: lastName,
			gender: gender || user.gender,
			document: cpf || user.cpf,
			phone: phone || user.phone
		});

		return this.patch('/dataentities/CL/documents', data);
	}
}

module.exports = VtexService;
