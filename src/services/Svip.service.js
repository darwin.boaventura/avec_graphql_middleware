const {RESTDataSource} = require('apollo-datasource-rest');
const helpers = {
	object: require('../helpers/Object.helper'),
	string: require('../helpers/String.helper')
};

class SalaoVipService extends RESTDataSource {
	constructor() {
		super();

		this.baseURL = process.env.SALAO_VIP_API_URI;
	}

	getUserByEmail(email) {
		const data = {
			salaoAcessoSolicita: process.env.SALAO_VIP_ACESSO_SOLICITA,
			email: email
		};

		return this.post('/avecshop/cliente/busca', data);
	}

	createUser({ name, gender, cpf, phone, email, password }) {
		const data = helpers.object.removeEmptyKeys({
			salaoAcessoSolicita: process.env.SALAO_VIP_ACESSO_SOLICITA,
			jwt_cliente: process.env.SALAO_VIP_JWT_CLIENT,
			nome: name,
			email: email,
			cpf: cpf,
			sexo: gender,
			telefone: phone,
			celular: phone,
			senha: password
		});

		return this.post('/avecshop/cliente', data);
	}

	updateUser({ userId, tokenSVIP, name, gender, cpf, phone, password }) {
		const data = helpers.object.removeEmptyKeys({
			nome: name,
			sexo: gender,
			cpf: cpf,
			senha: password,
			telefone: phone,
			celular: phone
		});

		return this.put(`/cliente/${userId}`, data, { headers: { "Authorization": tokenSVIP }});
	}

	updateUserPicture({ photo, token }) {
		return this.post('/cliente/foto', { file: photo }, { headers: { Authorization: token } });
	}

	dispatchEmailVerification({ userId, email })  {
		return this.get('/cliente/verificar/email', { cliente_id: userId, email: email });
	}

	validateEmailVerificationCode({ code, email }) {
		return this.post('/cliente/verificar/email', { codigo: code, email: email });
	}

	dispatchEmailVerification({ userId, phone })  {
		return this.get('/cliente/verificar', { cliente_id: userId, celular: helpers.string.leaveOnlyNumberFromPhoneNumber({phone})});
	}

	validateMessageVerificationCode({ code, phone }) {
		return this.post('/cliente/verificar', { codigo: code, celular: helpers.string.leaveOnlyNumberFromPhoneNumber({phone}) });
	}

	login({email, password}) {
		const data = {
			jwt_cliente: process.env.SALAO_VIP_JWT_CLIENT,
			user: email,
			password: password
		};

		return this.post('/avecshop/cliente/signin', data);
	}

	loginSocialNetwork( { token, provider, email, name, gender } ) {
		const data = helpers.object.removeEmptyKeys({
			jwt_cliente: process.env.SALAO_VIP_JWT_CLIENT,
			salaoAcessoSolicita: process.env.SALAO_VIP_ACESSO_SOLICITA,
			social_token: token,
			social_provider: provider,
			nome: name,
			email: email,
			sexo: gender
		});

		return this.post('/cliente/signin/social', data);
	}
};

module.exports = SalaoVipService;
