const jwt = require('jwt-simple');

const TokenController = {
	decodeToken: (token) => {
		return jwt.decode(token, process.env.SERVER_JWT_KEY);
	},

	gerenateToken: (user) => {
		return jwt.encode({ id: user.id, nome: user.nome, email: user.email, sexo: user.sexo, cpf: user.cpf, telefone: user.telefone, token: user.token, generatedIn: new Date() }, process.env.SERVER_JWT_KEY);
	}
};

module.exports = TokenController;
