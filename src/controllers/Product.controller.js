const ProductController = {
	convertDataToImagePayload(image) {
		return {
			id: image.imageId,
			uri: image.imageUrl,
			name: image.imageText
		};
	},

	convertDataToPricePayload(prices) {
		return {
			original: prices.PriceWithoutDiscount,
			withDiscount: prices.Price,
			discounts: prices.DiscountHighLight,
			list: prices.ListPrice
		};
	},

	convertDataToInstallmentPayload (installment) {
		return {
			value: installment.Value,
			name: installment.Name,
			interestRate: installment.interestRate,
			valueWithInterestRate: installment.valueWithInterestRate,
			numberOfInstallments: installment.paymentSystemName,
			paymentSystemName: installment.paymentSystemName,
			paymentSystemGroupName: installment.paymentSystemGroupName,
		};
	},

	convertDataToSellerPayload(seller) {
		return {
			id: seller.sellerId,
			name: seller.sellerName,
			isDefaultSeller: seller.sellerDefault,
			installments: seller.commertialOffer.Installments && seller.commertialOffer.Installments.length ? seller.commertialOffer.Installments.map((installment) => this.convertDataToInstallmentPayload(installment)) : [],
			price: this.convertDataToPricePayload(seller.commertialOffer),
			gifts: seller.commertialOffer.GiftSkuIds,
			availableQuantity: seller.commertialOffer.AvailableQuantity,
			tax: seller.commertialOffer.Tax
		};
	},

	convertDataToItemPayload(item) {
		return {
			id: item.itemId,
			name: item.name,
			fullName: item.nameComplete,
			measurementUnit: item.measurementUnit,
			unitMultiplier: item.unitMultiplier,
			images: item.images.map((image) => this.convertDataToImagePayload(image)),
			sizes: item.Tamanho,
			variations: item.variations,
			sellers: item.sellers.map((seller) => this.convertDataToSellerPayload(seller))
		};
	},

	convertVtexPayloadToSchema(responseItem) {
		if (responseItem) {
			return {
				id: responseItem.productId,
				name: responseItem.productName,
				fullName: responseItem.items[0].nameComplete,
				uri: responseItem.link,
				seller: responseItem.items[0].sellers[0].sellerName,
				referenceCode: responseItem.items[0].referenceId ? responseItem.items[0].referenceId[0].Value : undefined,
				images: responseItem.items[0].images && responseItem.items[0].images.length ? responseItem.items[0].images.map((image) => this.convertDataToImagePayload(image)) : [],
				description: responseItem.description,
				categories: responseItem.categoriesIds,
				price: responseItem.items[0].sellers && responseItem.items[0].sellers.length ? this.convertDataToPricePayload(responseItem.items[0].sellers[0].commertialOffer) : [],
				items: responseItem.items && responseItem.items.length ? responseItem.items.map((item) => this.convertDataToItemPayload(item)) : []
			};
		}
	},

	generateQueryStringsFilters(filters) {
		let filterString = '';

		if (filters.categoryId) {
			filterString += `C:${filters.categoryId}`;
		}

		if (filters.brandId) {
			filterString += `,brandId:${filters.brandId}`;
		}

		if (filters.specificationId) {
			filterString += `,specificationFilter_${filters.specificationId}`;
		}

		if (filters.specificationId) {
			filterString += `,productClusterIds:${filters.collectionId}`;
		}

		if (filters.priceRange && filters.priceRange.minPrice && filters.priceRange.maxPrice) {
			filterString += `,P:[${filters.priceRange.minPrice} TO ${filters.priceRange.maxPrice}]`;
		}

		if (filters.salesChannel) {
			filterString += `,isAvailablePerSalesChannel_${filters.salesChannel}`;
		}

		if (filters.sellerId) {
			filterString += `,sellerIds:${filters.sellerId}`;
		}

		return filterString;
	},

	gerenateQueryStringsPagination(pagination) {
		if (pagination) {
			if (String(pagination.from) && String(pagination.to)) {
				return {
					'_from': pagination.from,
					'_to': pagination.to
				};
			}
		}
	}
};

module.exports = ProductController;
