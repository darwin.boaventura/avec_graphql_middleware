const controllers = {
	token: require('./Token.controller')
};
const services = {
	token: require('../services/Token.service')
};

const UserController = {
	verifyIfHasTokenAndUser: ({ user, token }) => {
		if (token && user) {
			if (!user.id) {
				throw new Error('Token inválido!');
			}
		} else {
			throw new Error(`O usuário não está logado.`);
		}
	},

	verifyIfTokenHasExpired: async ({ user, token }) => {
		const getTokenFromDatabase = await services.token.getToken({ userId: user.id, token });

		if (!getTokenFromDatabase) {
			throw new Error(`Token expirado, faça o login novamente.`);
		}
	},

	handleSuccessfulLogin: async (req, dataSources) => {
		const userSVIP = req.data.client;
		const tokenSVIP = req.data.token;

		const token = await controllers.token.gerenateToken(Object.assign({}, userSVIP, { token: tokenSVIP }));

		await dataSources.token.saveToken(userSVIP.id, token);

		return {
			success: true,
			message: 'Login efetuado com sucesso!',
			token: token
		};
	}
};

module.exports = UserController;
