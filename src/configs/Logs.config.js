const morgan = require('morgan');

module.exports = (file) => {
	return morgan('DATA: :date[web] | :method: :url | RESPONSE CODE: :status ', { stream: file });
};
