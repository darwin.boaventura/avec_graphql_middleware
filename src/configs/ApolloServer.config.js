const typeDefs = require('../Schema');
const resolvers = require('../Resolvers');

const services = {
	salonVip: require('../services/Svip.service'),
	token: require('../services/Token.service'),
	vtex: require('../services/Vtex.service'),
	list: require('../services/List.service'),
	indication: require('../services/Indication.service')
};
const controllers = {
	token: require('../controllers/Token.controller')
};

const dataSources = {
	svip: new services.salonVip(),
	vtex: new services.vtex(),
	token: services.token,
	list: services.list,
	indication: services.indication
};

module.exports = {
	typeDefs,
	resolvers,
	dataSources: () => dataSources,
	context: ({req}) => {
		const token = req.headers.authorization || undefined;
		const user = token ? controllers.token.decodeToken(token) : {};

		return {
			token,
			user
		};
	}
};
