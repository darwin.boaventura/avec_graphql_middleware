const mongoose = require('mongoose');

mongoose.Promise = require('bluebird');
mongoose.set('useCreateIndex', true);

module.exports = (() => { mongoose.connect(`mongodb://${process.env.DATABASE_URI}/${process.env.DATABASE_NAME}`, { useNewUrlParser: true }); return mongoose.connection; })();
