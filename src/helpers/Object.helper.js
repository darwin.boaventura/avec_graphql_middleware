const _ = require('lodash');

module.exports = {
	removeEmptyKeys(fields) {
		for (let key in fields) {
			if (fields[key] === undefined || fields[key] === null || (_.isArray(fields[key]) && fields[key].length < 1)) {
				delete fields[key];
			}
		}

		return fields;
	}
};
