const StringHelper = {
	leaveOnlyNumberFromPhoneNumber: ({ phone }) => {
		return phone.replace(' ', '').replace(' ', '').replace('(', '').replace(')', '').replace('-', '');
	}
};

module.exports = StringHelper;
