const _ = require('lodash');

module.exports = {
	sliceData: (data, limit, offset = 0) => {
		return limit && data && data.length ? _.slice(data, offset, (offset + limit)) : data;
	},

	asyncForEach: async(array, callback) => {
		for (let index = 0; index < array.length; index++) {
			try {
				await callback(array[index], index, array);
			} catch(e) {
				console.log('Error:', e);
			}
		}
	}
};
