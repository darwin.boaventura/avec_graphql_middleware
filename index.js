require('dotenv').config();

const { ApolloServer } = require('apollo-server-express');
const fs = require('fs');
const app = require('express')();
const path = require('path');
const logs = require('./src/configs/Logs.config');
const mongo = require('./src/configs/Mongoose.config');
const config = require('./src/configs/ApolloServer.config');
const server = new ApolloServer(config);
const logFile = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' });

app.use(logs(logFile));

server.applyMiddleware({app});

mongo.once('open', () => {
	return app.listen(process.env.SERVER_PORT, () => {
		console.log(`The server is listening on port ${process.env.SERVER_PORT}`);
	});
});
